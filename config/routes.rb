Rails.application.routes.draw do
    devise_for :admins, controllers:{
        sessions: 'admins/sessions'  
}
    devise_for :users, controllers:{
        sessions: 'users/sessions'
}
    
  resources :juegos 
    
    
    devise_scope :admin do
        get "admins/users/list",
        to: "admins/sessions#list",
        as: "list"
    end
    root to: 'juegos#index'
end
