json.extract! juego, :id, :nombre, :descripcion, :desarrollador, :plataforma, :modojuego, :imagen, :created_at, :updated_at
json.url juego_url(juego, format: :json)
