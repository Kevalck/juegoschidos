class Juego < ApplicationRecord
    has_attached_file :image, style: { thumb: '120x120', large: '300x400' }, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
