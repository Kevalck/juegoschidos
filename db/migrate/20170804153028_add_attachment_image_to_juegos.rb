class AddAttachmentImageToJuegos < ActiveRecord::Migration[5.0]
  def self.up
    change_table :juegos do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :juegos, :image
  end
end
